# Angular 6 Doc
## Vista General

Angular es una plataforma y un framework para construir aplicaciones del lado del cliente en HTML y TypeScript. Angular es en si mismo escrito en TypeScript. Implementa funcionalidad core y opcional como un conjunto de librerias de TypeScript que puedes importa en tus aplicaciones.

Los bloques basicos de construcción de una aplicación Angular son los *NgModules*, que proveen un cntexto de compilación para *components*. Los NgModules reunen código relacionado dentro de conjuntos funcionales; una aplicación Angular es definida por un conjunto de NgModules. Una aplicación siempre tiene al menos un *root module* que permita el arranque, y generalmente tiene muchos mas modulos de caracteristicas.

* Los components definen vistas, que son conjuntos de elementos de pantalla que Angular puede escoger y modificar de acuerdo a la lógica e información de tu programa. Todas las aplicaciones tienen al menos un root component.
* Los components utilizan *services*, que proveen una funcionalidad especifica y que no esta directamente relacionada con las vistas. Los *Service providers* pueden ser inyectados dentro de los components como dependencias, haciendo tu código modular, reusable, y eficiente.

Tanto componentes como servicios son simples clases con *decorators* que marcan su tipo y proveen metadata que le dice a Angular como utilizarlos.

* La metadata para una clase component se asocia con un *template* que define una vista. Un template ombina HTML común Angular *directives* y *binding markup* que permiten a Angular modificar el HTML anes de la renderización.
*La metadata para una clase service provee la información que Angular necesita para hacerlo disponible a los components a traves de la *Inyección de dependencias* (DI).

Un component de aplicación generalmente define varias vistas, ordenadas jerarquicamente. Angular provee el servicio *Router* para ayudarte a definir los paths de navegación entre las vistas. El router provee sofisticadas capacidades de navegación en el navegador.

### Modules
Angular define el NgModule, que difiere y complementa al módulo de JavaScript (ES2015). Un NgModule declara un contexto de compilación para un conjunto de componentes que estan dedicados a un dominio de aplicación, un flujo de trabajo, o un conjunto de capacidades estrechamente relacionadas. Un NgModule puede asociar sus componentes con código relacionado, como services, para formar unidades funcionales.

Todas las aplicaciones Angular tienen un root module, convencionalmente llamado *AppModule*, que provee el mecanismo de arranque que comienza la aplicación. Una aplicación generalmente contiene muchos módulos funcionales.

Como los módulos JavaScript, los NgModules pueden importar funcionalidad de otros NgModules, y permite que su propia funcionalidad sea exportada y utilizada or otros NgModules. Por ejemplo, para utilizar el servicio router en tu app, importas el NgModule Router.

Organizando tu código en distintos módulos funcionales ayudas en la administración del desarrollo de aplicaciones complejas, y diseñandola para reusabilidad. Ademas, esta técnica te permite omar ventaja de *lazy-loading* - que es, carga de modulos bajo demanda - con el fin de minimizar la cantidad de código que necesita ser cargado en el inicio.

### Components
Todas las aplicaciones Angular tienen al menos un component, el root component que conecta una jerarquia de componentes con el DOM de la página. Cada componente define una clase que contiene la lógica y la información de la aplicación, y es asociada con un template HTML que define una vista para ser mostrada en el ambiente seleccionado.

El decorator *@Component* identifica la clase inmediatamente debajo como un component, y provee el template y la metadata especifica de los components relacionados. 
```
Los decorators son funciones que modifican las clases JavaScript. Angular defne un numer de decorators que asocian tipos especificos de metadata a las clases, para conocer que significan esas clases y como deben trabajar.
```

### Templates, directives y data binding
Un template combina etiquetas HTML con Angular que puede modificar los elementos HTML antes de que estos sean desplegados. Los directives de template proveen lógica de programa, y el binding markup conecta la información de tu aplicación y el DOM.

* Event binding le permite a tu aplicación responder a la entrada del usuario en el ambiente seleccionado actualizando la información de tu aplicación.
* Property binding te permite interpolar valores que son calculados de la informacion de tu aplicación en el HTML.

Antes de que una vista sea desplegada, Angular evalua las directivas y resuelve la sintaxis binding en el template para modificar los elementos HTML y el DOM, de acuerdo a la lógica e información de tu programa. Angular soporta *two-way data binding*, lo que siginifica que los cambios en el DOM, como las selecciones del usuario, tambien pueden reflejarse en los datos de tu programa.

Tus templates tambien pueden utilizar *pipes* para mejorar la experiencia de usuaro transformando los valores a desplegar. Utiliza los pipes para desplegar, por ejemplo, valores de fecha y moneda de un modo apropiado a la configuración del usuario. Angular provee pipes predefinidos para transformaciones comunes, y puedes tambien definir las propias.

### Service e Inyección de Dependencias
Para datos o lógica que no esta asociada con una vista especifica, y que quieres compartir entre components, crea una clase service. Una definición de una clase service esta inmediatamente precedida por el decorator *@Injectable*. El decorator provee la metadata que permite a tu servicio ser inyectado en los components clientes como una dependencia.

La inyección de dependencias (DI) te permite mantener tus clases component delgadas y eficientes. Ellos no buscan los datos desde el servidor, validan inputs del usuario, o loguean directamente en la consola; Ellos delegan tales tareas a los servicios.

### Routing
El NgModule Angular Router provee un servicio que te permite definir un path de navegación entre los diferentes estados y vistas de la aplicación. Se basa en las convenciones familiares de navegación del navegador: 
* Ingresa una URL en la barra de direcciones y el navegador navega a la página correspondiente.
* Haz click en links de la página y el navegador navega a una nueva página.
* Haz click en los botones "atras" y "adelante" del navegador y el navegador navega hacia atrás o hacía adelante a través del historial de páginas que has visto.

El router mapea rutas tipo URL a vistas en vez de páginas. Cuando un usuario ejecuta una acción, como clickear un link, que cargaría una nueva página en el navegador, el router intercepta el comportamiento del navegador, y muestra o esconde jerarquía de vistas.

Si el router determina que el estado de la aplicación actual requiere una funcionalidad particular, y el módulo que define no ha sido cargado, el router puede lazy-loading el modulo bajo demanda.

El router interpreta un link URL de acuerdo a las reglas de navegación de las vistas y el estado de los datos de tu aplicación. Puedes navegar a nuevas vistas cuando el usuario clickea un botón, selecciona desde un drop box, o en respuesta a algún otro estimulo de cualquier fuente. El router registra la actividad en el historial del navegador, así que los botones de atrás y adelante tambien funcionan.

Para definir las reglas de navegación, asocias los paths de navegación con tus components. Un path utiliza una sintaxis como URL que integra los datos de tu programa, de manera muy parecida en la que la sintaxis de template integra tus vistas con tus datos de programa. Después puedes aplicar lógica de programa para escoger que vistas mostrar o esconder, en respuesta a un input de usuario y tus propias reglas de acceso 