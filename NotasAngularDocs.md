Basic building blocks of Angular app are NgModules (provide compilation context for components)
    NgModules colect related code into functional sets
    An Angular app is defined by a set of NgModules
    An app always has at least a root module that enables bootstrapping

Components define views, which are sets of screen elements that Angular can choose among and
modify according to your program logic and data. Every app has at least a root component

Components use services, wich provide specific functionality not directly related to views.

Both components and services are simply classes, with decorators that mark their type and provide
metadata that tells Angular how to use them.
    The metadata for a component class associates it with a template that defines a view
    The metadata for a service class provides the information Angular needs to make it 
    available to components through Dependency Injection